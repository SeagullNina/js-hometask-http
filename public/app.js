'use strict';
function addUserFetch(userInfo) {
  // напишите POST-запрос используя метод fetch
  let options = {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify(userInfo)
};
return fetch('/users', options)
    .then(response => {
        if (response.status === 201) 
        {
            return Promise.resolve(response);
        } 
        else 
        {
            return Promise.reject(response.status + "  " + response.statusText);
        }
    })
    .then(response => response.json())
    .then(user => Promise.resolve(user.id));
}

function getUserFetch(id) {
  // напишите GET-запрос используя метод fetch
  fetch(`/users/${id}`)
  .then(function(response) {
    if (response.status === 201) 
        {
            return Promise.resolve(response);
        } 
        else 
        {
            return Promise.reject(response.status + "  " + response.statusText);
        }
})
.then(response => response.json())
.then(obj => Promise.resolve(obj));
}

function addUserXHR(userInfo) {
  // напишите POST-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onloadend = function () 
    {
      if (xhr.status === 201)
      resolve(JSON.parse(xhr.responseText).id);
      else
      reject(xhr.status + " " + xhr.message);
    };
    xhr.open('POST', `users/`);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(JSON.stringify(userInfo));
  });
}


function getUserXHR(id) {
  // напишите GET-запрос используя XMLHttpRequest
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.onloadend = function () 
    {
      if (xhr.status === 201)
      resolve(JSON.parse(xhr.responseText));
      else
      reject(xhr.status + " " + xhr.message);
    };
    xhr.open('GET', `users/${id}`);
    xhr.send();
  });
}

function checkWork() {  
  addUserXHR({ name: "Alice", lastname: "FetchAPI" })
    .then(userId => {
      console.log(`Был добавлен пользователь с userId ${userId}`);
      return getUserXHR(userId);
    })
    .then(userInfo => {
      console.log(`Был получен пользователь:`, userInfo);
    })
    .catch(error => {
      console.error(error);
    });
}

 checkWork();